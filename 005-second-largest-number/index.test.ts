
import { secondLargestNumber } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('secondLargestNumber()', () => {
    it('should return 40 as second largest number', function() { 
        const numbers = [10, 40, 30, 20, 50]
        
        expect( secondLargestNumber(numbers) ).toBe(40)
    }) 
    
    it('should return 105 as second largest number', function() { 
        const numbers = [25, 143, 89, 13, 105]
        
        expect( secondLargestNumber(numbers) ).toBe(105)
    }) 
    
    it('should return 23 as second largest number', function() { 
        const numbers = [54, 23, 11, 17, 10]
        
        expect( secondLargestNumber(numbers) ).toBe(23)
    }) 
    
    it('should return 0 as there is no second largest number', function() { 
        const numbers = [1, 1]
        
        expect( secondLargestNumber(numbers) ).toBe(0)
    }) 
    
    it('should return 1 as there is only one number', function() { 
        const numbers = [1]
        
        expect( secondLargestNumber(numbers) ).toBe(1)
    }) 

    it('should return 0 as there is no number', function() { 
        const numbers = []
        
        expect( secondLargestNumber(numbers) ).toBe(0)
    }) 

    it('should throw error for not receiving valid array input', function() { 
        
        expect( () => secondLargestNumber(2) ).toThrow("Not valid input received")
    })

    it('should throw error for not receiving valid array input', function() { 
        
        expect( () => secondLargestNumber("Numbers") ).toThrow("Not valid input received")
    })
}) 






/*
secondLargest([10, 40, 30, 20, 50]) ➞ 40 
 
secondLargest([25, 143, 89, 13, 105]) ➞ 105 
 
secondLargest([54, 23, 11, 17, 10]) ➞ 23 
 
secondLargest([1, 1]) ➞ 0 
 
secondLargest([1]) ➞ 1 
 
secondLargest([]) ➞ 0 
*/