export function secondLargestNumber(numbersArray) {
    let secondLargest = 0
    let largest = 0

    if (!numbersArray || !Array.isArray(numbersArray)) {
        throw new Error("Not valid input received")
    }

    if (numbersArray.length === 1) {
        return numbersArray[0]
    }
    else if (numbersArray.length === 2) {
        if (numbersArray[0] > numbersArray[1]) return numbersArray[1]
        else if (numbersArray[1] > numbersArray[0]) return numbersArray[0]
        else return 0
    }
    else if (numbersArray.length === 0) {
        return 0
    }

    for (let i = 0; i < numbersArray.length; i++) {
        if (numbersArray[i] > largest) {
            secondLargest = largest
            largest = numbersArray[i]
        }
        else if (numbersArray[i] > secondLargest) {
            secondLargest = numbersArray[i]
        }
    }
    return secondLargest
}


// SOME MORE CLEVER SOLUTIONS
/*
    Using sort() to sort in ascending order and taking the second highest number/index
    Using Math.max.apply() to find the highest number, take that out of the array then returning the new highest(secondHighest) number
*/