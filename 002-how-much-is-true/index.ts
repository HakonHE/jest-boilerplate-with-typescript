// SOURCE CODE

export function countTrue(bools) {
    let counter = 0
    
    for (let i = 0; i < bools.length; i++) {
        if (bools[i] === true) {
            counter++
        }
    }
    return counter
}