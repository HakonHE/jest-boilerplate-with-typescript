import { countTrue } from "./index" // ES Module

// TEST CODE
it('should find 2 trues', function() { 
    const bools = [true, false, false, true, false]
    
    expect( countTrue(bools) ).toBe(2)
}) 

it('should find 0 trues', function() { 
    const bools = [false, false, false, false]
    
    expect( countTrue(bools) ).toBe(0)
}) 

it('should find 1 true', function() { 
    const bools = [1, 0, 0, true, false]
    
    expect( countTrue(bools) ).toBe(1)
}) 

it('should find 1 true', function() { 
    const bools = ["true", 1, true, 0, false]
    
    expect( countTrue(bools) ).toBe(1)
}) 

it('should find 0 trues', function() { 
    const bools = []
    
    expect( countTrue(bools) ).toBe(0)
}) 

