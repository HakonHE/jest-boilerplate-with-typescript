import { elasticize } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('elasticize()', () => {
    it('should return a string of ANNNNA', function() { 
        const expectedValue = "ANNNNA"
        
        expect(elasticize("ANNA")).toEqual(expectedValue)
    }) 
    
    it('should return a string of KAAYYYAAK', function() { 
        const expectedValue = "KAAYYYAAK"
        
        expect(elasticize("KAYAK")).toEqual(expectedValue)
    }) 
    
    it('should return a string of only X', function() {  
        const expectedValue = "X"
        
        expect(elasticize("X")).toEqual(expectedValue)
    })
    
    it('should return a string of ANNAAANNNAAS', function() { 
        const expectedValue = "ANNAAANNNAAS"
        
        expect(elasticize("ANANAS")).toEqual(expectedValue)
    }) 
    
}) 



/* 
elasticize("ANNA") ➞ "ANNNNA" 
 
elasticize("KAYAK") ➞ "KAAYYYAAK" 
 
elasticize("X") ➞ "X"


*/