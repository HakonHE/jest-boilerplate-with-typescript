export function elasticize(word) {
    let left = ""
    let right = ""
    let center = ""
    let finalWord = ""
    let counter = 1
    const wordLength = word.length
    
    if (!word) {
        throw new Error("Didn't get a word")
    }

    if (wordLength < 3) return word

    if (wordLength % 2 === 0) {
        left = word.substring(0, wordLength/2)
        right = word.substring(wordLength/2, wordLength)
        for (let i = 0; i < left.length; i++) {
            finalWord += left[i].repeat(counter)
            counter++
        }
        counter = wordLength/2
        for (let j = 0; j < right.length; j++) {
            finalWord += right[j].repeat(counter)
            counter--
        }
    } else {
        left = word.substring(0, (wordLength/2))
        center = word.substring((wordLength/2), (wordLength/2)+1)
        right = word.substring(wordLength/2+1)
        
        for (let i = 0; i < left.length; i++) {
            finalWord += left[i].repeat(counter)
            counter++
        }
        finalWord += center.repeat(counter)
        counter = Math.floor((wordLength/2))
        for (let j = 0; j < right.length; j++) {
            finalWord += right[j].repeat(counter)
            counter--
        }
    }
    
    return finalWord
}