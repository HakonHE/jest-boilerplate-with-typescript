export function maximumProfit(numberArray) {
    let maximumProfit = 0
    for (let i = 0; i < numberArray.length; i++) {
        for (let j = i + 1; j < numberArray.length; j++) {
            if((numberArray[j] - numberArray[i]) > maximumProfit) {
                maximumProfit = numberArray[j] - numberArray[i]
            }
        }
    }
    return maximumProfit
}