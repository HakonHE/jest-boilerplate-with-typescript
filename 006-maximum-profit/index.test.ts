
import { maximumProfit } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('maximumProfit()', () => {
    it('should return 14 as maximum profit', function() { 
        const numbers = [8, 5, 12, 9, 19, 1]
        
        expect( maximumProfit(numbers) ).toBe(14)
    }) 
    
    it('should return 7 as maximum profit', function() { 
        const numbers = [2, 4, 9, 3, 8]
        
        expect( maximumProfit(numbers) ).toBe(7)
    }) 
    
    it('should return 0 as maximum profit', function() { 
        const numbers = [21, 12, 11, 9, 6, 3]
        
        expect( maximumProfit(numbers) ).toBe(0)
    }) 

    it('should return 3 as maximum profit', function() { 
        const numbers = [21, 12, 11, 14, 6, 5]
        
        expect( maximumProfit(numbers) ).toBe(3)
    })
    
    it('should return 0 as maximum profit', function() { 
        const numbers = [2, 1]
        
        expect( maximumProfit(numbers) ).toBe(0)
    })
    
    it('should return 0 as maximum profit', function() { 
        const numbers = [3]
        
        expect( maximumProfit(numbers) ).toBe(0)
    }) 
}) 


/*
Examples 
 
maximumProfit([8, 5, 12, 9, 19, 1]) -> 14 
 
maximumProfit([2, 4, 9, 3, 8]) -> 7 
 
maximumProfit([21, 12, 11, 9, 6, 3]) -> 0

*/