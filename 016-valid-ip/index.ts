/**
 * Takes a possible ip string and returns a boolean for if it is or not.
 * @param ipString 
 * @returns boolean
 */
export function isValidIP(ipString) {
    let validIp = []
    const test = [false]
    const numberArray = ipString.split(".")

    if(numberArray.length !== 4) return false
    
    validIp = numberArray.map((number) => {
        if(number.length > 3) return false
        else if (number !== removeLeadingZero(number)) return false
        else if (Number(number) > 255 || Number(number) < 1) return false
    })

    return !validIp.includes(false)
}

/**
 * Removes if there is a leading zero.
 * @param str : string 
 * @returns str : string without leading zero
 */
function removeLeadingZero(str) {
    const regex = "0"
    return str.replace(regex , "")
}