import { isValidIP } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('isValidIP()', () => {
    it('should return true', function() { 
        const input = "1.2.3.4"
        const output = true
        
        expect(isValidIP(input)).toEqual(output)
    }) 
    
    it('should return false', function() { 
        const input = "1.2.3"
        const output = false 
        
        expect(isValidIP(input)).toEqual(output)
    }) 
    
    it('should return false', function() { 
        const input = "1.2.3.4.5"
        const output = false
        
        expect(isValidIP(input)).toEqual(output)
    })

    it('should return true', function() { 
        const input = "123.45.67.89"
        const output = true
        
        expect(isValidIP(input)).toEqual(output)
    }) 
    
    it('should return false', function() { 
        const input = "123.456.78.90"
        const output = false
        
        expect(isValidIP(input)).toEqual(output)
    }) 

    it('should return false', function() { 
        const input = "123.045.067.089"
        const output = false
        
        expect(isValidIP(input)).toEqual(output)
    }) 
}) 

/*
isValidIP("1.2.3.4") ➞ true 
 
isValidIP("1.2.3") ➞ false 
 
isValidIP("1.2.3.4.5") ➞ false 
 
isValidIP("123.45.67.89") ➞ true 
 
isValidIP("123.456.78.90") ➞ false 
 
isValidIP("123.045.067.089") ➞ false

*/