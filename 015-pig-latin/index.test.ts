import { pigLatinSentence } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('pigLatinSentence()', () => {
    it('should return a string of "isthay isway igpay atinlay"', function() { 
        const input = "this is pig latin"
        const output = "isthay isway igpay atinlay"
        
        expect(pigLatinSentence(input)).toEqual(output)
    }) 
    
    it('should return a string of "allway eetstray ournaljay"', function() { 
        const input = "wall street journal"
        const output = "allway eetstray ournaljay"
        
        expect(pigLatinSentence(input)).toEqual(output)
    }) 
    
    it('should return a string of "aiseray ethay idgebray"', function() { 
        const input = "raise the bridge"
        const output = "aiseray ethay idgebray"
        
        expect(pigLatinSentence(input)).toEqual(output)
    }) 
    
    it('should return a string of "allway igspay oinkway"', function() {  
        const input = "all pigs oink"
        const output = "allway igspay oinkway"
        
        expect(pigLatinSentence(input)).toEqual(output)
    })

    
}) 

/*
pigLatinSentence("this is pig latin") ➞ "isthay isway igpay atinlay" 
 
pigLatinSentence("wall street journal") ➞ "allway eetstray ournaljay" 
 
pigLatinSentence("raise the bridge") ➞ "aiseray ethay idgebray" 
 
pigLatinSentence("all pigs oink") ➞ "allway igspay oinkway"
*/