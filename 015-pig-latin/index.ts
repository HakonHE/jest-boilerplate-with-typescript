/**
 * Takes a sentence and tranlates it to pig latin.
 * @param sentence 
 * @returns String sentence in pig latin
 */
export function pigLatinSentence(sentence) { 
    const wordArray = sentence.split(" ");
    const vowelFirst = wordArray.map((word) => checkVowelIndex(word));
    
    for (let i = 0; i < wordArray.length; i++) {
        if ( vowelFirst[i] === 0) {
            wordArray[i] += "way";
        }
        else {
            wordArray[i] = 
                wordArray[i].slice(vowelFirst[i]) + 
                wordArray[i].slice(0, vowelFirst[i]) + "ay";
        }
    }

    return wordArray.join(" ");
}

/**
 * Takes a word and returns the index of the vowel
 * @param word 
 * @returns Array of vowel index
 */
function checkVowelIndex(word) {
    const vowels = /[aeiou]/gi;
    return vowels.exec(word).index;
}