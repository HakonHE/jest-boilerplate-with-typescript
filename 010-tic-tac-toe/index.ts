export function winnerPicker(gameboard) {
    const a1 = gameboard[0][0], a2 = gameboard[0][1], a3 = gameboard[0][2],
    b1 = gameboard[1][0], b2 = gameboard[1][1], b3 = gameboard[1][2],
    c1 = gameboard[2][0], c2 = gameboard[2][1], c3 = gameboard[2][2]

    if ((a1 === "X" && a2 === "X" &&  a3 === "X") || (a1 === "X" && b1 === "X" &&  c1 === "X") ||
    (a1 === "X" && b2 === "X" &&  c3 === "X") || (a2 === "X" && b2 === "X" &&  c2 === "X") || 
    (a3 === "X" && b3 === "X" &&  c3 === "X") || (a3 === "X" && b2 === "X" &&  c1 === "X") || 
    (b1 === "X" && b2 === "X" &&  b3 === "X") || (c1 === "X" && c2 === "X" &&  c3 === "X")) {
        return "X"
    }
    else if ((a1 === "O" && a2 === "O" &&  a3 === "O") || (a1 === "O" && b1 === "O" &&  c1 === "O") ||
    (a1 === "O" && b2 === "O" &&  c3 === "O") || (a2 === "O" && b2 === "O" &&  c2 === "O") ||
    (a3 === "O" && b3 === "O" &&  c3 === "O") || (a3 === "O" && b2 === "O" &&  c1 === "O") ||
    (b1 === "O" && b2 === "O" &&  b3 === "O") || (c1 === "O" && c2 === "O" &&  c3 === "O")) {
        return "O"
    }
    

    return "DRAW"
}