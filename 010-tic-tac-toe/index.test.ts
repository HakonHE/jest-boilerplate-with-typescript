import { winnerPicker } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('winnerPicker()', () => {
    it('should return a X as X wins in this scenario', function() { 
        const input = [ 
            ["X", "O", "X"], 
            ["O", "X",  "O"], 
            ["O", "X",  "X"] 
          ]

        const output = "X"
        
        expect(winnerPicker(input)).toEqual(output)
    })
    
    it('should return an O as O wins in this scenario', function() { 
        const input = [ 
            ["O", "O", "O"], 
            ["O", "X", "X"], 
            ["E", "X", "X"] 
          ]

        const output = "O"
        
        expect(winnerPicker(input)).toEqual(output)
    }) 

    it('should return a X as X wins in this scenario', function() { 
        const input = [
            ["X", "X", "X"], 
            ["O", "O", ""], 
            ["", "", ""]
        ]

        const output = "X"
        
        expect(winnerPicker(input)).toEqual(output)
    }) 

    it('should return a X as X wins in this scenario', function() { 
        const input = [ 
            ["O", "O", "X"], 
            ["X", "", "X"], 
            ["O", "", "X"] 
          ]

        const output = "X"
        
        expect(winnerPicker(input)).toEqual(output)
    }) 

    it('should return a DRAW as no winner in this scenario', function() { 
        const input = [ 
            ["X", "X", "O"], 
            ["O", "O", "X"], 
            ["X", "X", "O"] 
          ]

        const output = "DRAW"
        
        expect(winnerPicker(input)).toEqual(output)
    }) 

    it('should return a DRAW as no winner in this scenario', function() { 
        const input = [ 
            ["X", "O", "O"], 
            ["O", "X", "X"], 
            ["X", "X", "O"] 
          ]

        const output = "DRAW"
        
        expect(winnerPicker(input)).toEqual(output)
    })
    
    it('should return an O as O wins in this scenario', function() { 
        const input = [ 
            ["O", "X", "O"], 
            ["O", "O", "X"], 
            ["X", "X", "O"] 
          ]

        const output = "O"
        
        expect(winnerPicker(input)).toEqual(output)
    })    

    it('should return a X as X wins in this scenario', function() { 
        const input = [ 
            ["O", "X", "O"], 
            ["O", "O", "X"], 
            ["X", "X", "X"] 
          ]

        const output = "X"
        
        expect(winnerPicker(input)).toEqual(output)
    })

})


/*
Examples 
ticTacToe([ 
  ["X", "O", "X"], 
  ["O", "X",  "O"], 
  ["O", "X",  "X"] 
]) ➞ "X" 
 
ticTacToe([ 
  ["O", "O", "O"], 
  ["O", "X", "X"], 
  ["E", "X", "X"] 
]) ➞ "O" 
 
ticTacToe([ 
  ["X", "X", "X"], 
  ["O", "O", ""], 
  ["", "", ""] 
]) ➞ "X" 
 
ticTacToe([ 
  ["O", "O", "X"], 
  ["X", "", "X"], 
  ["O", "", "X"] 
]) ➞ "X" 
 
 
ticTacToe([ 
  ["X", "X", "O"], 
  ["O", "O", "X"], 
  ["X", "X", "O"] 
]) ➞ "Draw"
*/