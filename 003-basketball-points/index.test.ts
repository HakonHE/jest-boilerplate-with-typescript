import { basketballPoints } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('points()', () => {
    it('should be 5 as final points', function() { 
        const points = [1, 1]
        
        expect( basketballPoints(points) ).toBe(5)
    }) 
    
    it('should be 29 as final points', function() { 
        const points = [7, 5]
        
        expect( basketballPoints(points) ).toBe(29)
    }) 
    
    it('should be 100 as final points', function() { 
        const points = [38, 8]
        
        expect( basketballPoints(points) ).toBe(100)
    }) 
    
    it('should be 3 as final points', function() { 
        const points = [0, 1]
        
        expect( basketballPoints(points) ).toBe(3)
    }) 
    
    it('should be 0 as final points', function() { 
        const points = [0, 0]
        
        expect( basketballPoints(points) ).toBe(0)
    }) 

    it('should throw error for not receiving valid two pointer input', function() { 
        
        expect( () => basketballPoints([undefined, undefined]) ).toThrow("No two pointers received")
    })

    it('should throw error for not receiving valid three pointer input', function() { 
        
        expect( () => basketballPoints([2, undefined]) ).toThrow("No three pointers received")
    })

    /*
    it('should throw if two pointers null', () =>
        expect(() => points(null, 5)).toThrow("No two pointers received")
    */
}) 
