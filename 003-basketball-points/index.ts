/**
 * Returning Basketball points.
 * @param {Array.<number>} points 
 * @returns {number}
 */
export function basketballPoints(points) {
    if (points[0] === undefined) {
        throw new Error("No two pointers received")
    }

    if (points[1] === undefined) {
        throw new Error("No three pointers received")
    }
    
    return points[0] * 2 + points[1] * 3
}


/*
if (twoPointers !== 0 && !twoPointers) {
    throw new Error("No two pointers received")
  }

  if (threePointers !== 0 && !threePointers) {
    throw new Error("No three pointers received")
  }
*/



// CONVOLUTED METHOD
/* 
let goalPoints = 2
for (let i = 0; i < points.length; i++) {
    finalPoints += points[i] * goalPoints
    goalPoints++
}

return finalPoints 
*/