import { splitOnDoubleLetter } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('splitOnDoubleLetter()', () => {
    it('should get let and ter separated', function() { 
        const expectedValue = ["let", "ter"]
        
        expect(splitOnDoubleLetter("Letter")).toEqual(expectedValue)
    }) 
    
    it('should get real and ly separated', function() { 
        const expectedValue = ["real", "ly"]
        
        expect(splitOnDoubleLetter("Really")).toEqual(expectedValue)
    }) 
    
    it('should get hap and py separated', function() {  
        const expectedValue = ["hap", "py"]
        
        expect(splitOnDoubleLetter("Happy")).toEqual(expectedValue)
    }) 
    
    it('should get shal and l separated', function() { 
        const expectedValue = ["shal", "l"]
        
        expect(splitOnDoubleLetter("Shall")).toEqual(expectedValue)
    }) 
    
    it('should get to and ol separated', function() { 
        const expectedValue = ["to", "ol"]
        
        expect(splitOnDoubleLetter("Tool")).toEqual(expectedValue)
    }) 

    it('should get mis, sis, sip and pi separated', function() { 
        const expectedValue = ["mis", "sis", "sip", "pi"]
        
        expect(splitOnDoubleLetter("Mississippi")).toEqual(expectedValue)
    }) 

    it('should get an empty array', function() { 
        const expectedValue = []
        
        expect(splitOnDoubleLetter("Easy")).toEqual(expectedValue)
    }) 

    it('should throw error for not receiving valid string input', function() { 
        
        expect( () => splitOnDoubleLetter(undefined) ).toThrow("No valid word given.!")
    })

    it('should throw if value received is invalid', function() {
        expect(() => splitOnDoubleLetter(null)).toThrow("No valid word given.!")
    })
}) 


/*
splitOnDoubleLetter(‘Letter’) -> [‘let’, ‘ter’] 
splitOnDoubleLetter(‘Really’) -> [‘real’, ‘ly’] 
splitOnDoubleLetter(‘Happy’) -> [‘hap’, ‘py’] 
splitOnDoubleLetter(‘Shall’) -> [‘shal’, ‘l’] 
splitOnDoubleLetter(‘Tool’) -> [‘to’, ‘ol’] 
splitOnDoubleLetter(‘Mississippi’) -> [‘mis’, ‘sis’, ‘sip’, ‘pi’] 
splitOnDoubleLetter(‘Easy’) returns []
*/