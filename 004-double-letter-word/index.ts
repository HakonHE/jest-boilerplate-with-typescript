export function splitOnDoubleLetter(word) {
    if(!word || !word.trim()) {
        throw new Error("No valid word given.!")
    }
    
    let index
    const lowerCaseWord = word.toLowerCase()

    for (let i = 1; i < word.length; i++) {
        if (word[i-1] === word[i]) {
            index = i
        }
    }

    if(index === undefined) {
        return [] 
    }
    else { 
        return [lowerCaseWord.slice(0, index), lowerCaseWord.slice(index)];
    }
}


// Dewalds version
/*

export function splitOnDoubleLetter(word) {


    if (!word || !word.trim()) {
        throw new Error('No word was given')
    }


    if (typeof word !== "string") {
        throw new Error("That's not a word! 💩")
    }
    
    let result = word[0]


    for (let i = 1; i < word.length; i++) {
        if (word[i -1] === word[i]) {
            result += ','
        }
        result += word[i]
    }


    const splitWords = result.split(',')


    if (splitWords.length === 1) { // No double letter! 😢
        return []
    } else {
        return splitWords
    }
}
*/