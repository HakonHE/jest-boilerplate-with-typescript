import { rearrangeSentence } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('rearrangeSentence()', () => {
    it('should return a string of "This is a Test"', function() { 
        const input = "is2 Thi1s T4est 3a"
        const output = "This is a Test"
        
        expect(rearrangeSentence(input)).toEqual(output)
    }) 
    
    it('should return a string of "For the good of the people"', function() { 
        const input = "4of Fo1r pe6ople g3ood th5e the2"
        const output = "For the good of the people"
        
        expect(rearrangeSentence(input)).toEqual(output)
    }) 
    
    it('should return a string of "JavaScript is so damn weird"', function() { 
        const input = "5weird i2s JavaScri1pt dam4n so3"
        const output = "JavaScript is so damn weird"
        
        expect(rearrangeSentence(input)).toEqual(output)
    }) 
    
    it('should return a string of ""', function() {  
        const input = " "
        const output = ""
        
        expect(rearrangeSentence(input)).toEqual(output)
    })

    it('should return a string of ""', function() {  
        const input = "         "
        const output = ""
        
        expect(rearrangeSentence(input)).toEqual(output)
    })
    
}) 


/*
rearrangeSentence("is2 Thi1s T4est 3a") ➞ "This is a Test" 
 
rearrangeSentence("4of Fo1r pe6ople g3ood th5e the2") ➞ "For the good of the 
people" 
 
rearrangeSentence("5weird i2s JavaScri1pt dam4n so3") ➞ "JavaScript is so damn weird" 
 
rearrangeSentence(" ") ➞ ""
*/