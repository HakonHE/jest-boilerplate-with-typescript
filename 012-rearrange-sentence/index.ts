export function rearrangeSentence(sentence) {
    const numberArray = sentence.match(/\d+/g)
    const newSentence = sentence.replaceAll(/\d+/g, "")
    const wordArray = newSentence.split(" ")
    let newArray = [...wordArray]

    if(!sentence || sentence.length === 0 || !sentence.replace(/\s/g, "").length) return ""

    for(let i = 0; i < newArray.length; i++) {
        const word = newArray[i]
        wordArray.splice(numberArray[i]-1, 1, word)
    }

    sentence = wordArray.slice(0, wordArray.length - 1).join(" ") + " " + wordArray.slice(-1)

    return sentence
}