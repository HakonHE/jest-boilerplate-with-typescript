import { groupInOrder } from "./index"

describe('groupInOrder()', () => {
    it('should return [[1, 3], [2, 4]] as output', function() { 
        const input = [1, 2, 3, 4]
        const output = [[1, 3], [2, 4]]
        
        expect( groupInOrder(input, 2) ).toEqual(output)
    }) 
    
    it('should return [[1, 3, 5, 7], [2, 4, 6]] as output', function() { 
        const input = [1, 2, 3, 4, 5, 6, 7]
        const output = [[1, 3, 5, 7], [2, 4, 6]]
        
        expect( groupInOrder(input, 4) ).toEqual(output)
    }) 
    
    it('should return [[1], [2], [3], [4], [5]] as output', function() { 
        const input = [1, 2, 3, 4, 5]
        const output = [[1], [2], [3], [4], [5]]
        
        expect( groupInOrder(input, 1) ).toEqual(output)
    }) 

    it('should return [[1, 3, 5], [2, 4, 6]] as output', function() { 
        const input = [1, 2, 3, 4, 5, 6]
        const output = [[1, 3, 5], [2, 4, 6]]
        
        expect( groupInOrder(input, 4) ).toEqual(output)
    })
    
})


/* 
group([1, 2, 3, 4], 2) ➞ [[1, 3], [2, 4]] 
 
group([1, 2, 3, 4, 5, 6, 7], 4) ➞ [[1, 3, 5, 7], [2, 4, 6]] 
 
group([1, 2, 3, 4, 5], 1) ➞ [[1], [2], [3], [4], [5]] 
 
group([1, 2, 3, 4, 5, 6], 4) ➞ [[1, 3, 5], [2, 4, 6]
*/