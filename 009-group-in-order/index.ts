/**
 * Gets an array of numbers and a size of array it wants the numbers
 * to be filled into. Fills up an array with the amount of arrays to fill
 * up with the size number. Then returns the array of arrays with numbers 
 * in them. 
 * @param Array.number numberArray 
 * @param number size 
 * @returns Array of arrays with numbers [[1,2],[3,4]]
 */
export function groupInOrder(numberArray, size) {
    const numberOfArrays = Math.ceil(numberArray.length / size)
    let trackNumber = 0
    const arrayOfNumberArrays = []

    for (let i = 0; i < numberArray.length; i++) {
        if(arrayOfNumberArrays.length < numberOfArrays) {
            arrayOfNumberArrays.push([])
        }
        arrayOfNumberArrays[trackNumber].push(numberArray[i])
        trackNumber++
        if(trackNumber === numberOfArrays) trackNumber = 0
    }

    return arrayOfNumberArrays
}