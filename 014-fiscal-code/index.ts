const MONTHS = { 1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "H", 7: 
"L", 8: "M", 9: "P", 10: "R", 11: "S", 12: "T" }

/**
 * Takes a person object and transform the info into a person id.
 * @param personObject Object with person info
 * @returns fiscalString String with fiscalcoded person id
 */
export function fiscalCoded(personObject) {
    let fiscalString = ""

    fiscalString += fiscalSurname(personObject.surname)
    fiscalString += fiscalName(personObject.name)
    fiscalString += fiscalDob(personObject.dob, personObject.gender)


    return fiscalString
}

/**
 * Takes the surname of the person object and transforms the info to fiscal coding.
 * @param surname 
 * @returns surnameCode String with surname code portion
 */
function fiscalSurname(surname) {
    const vowels = surname.match(/[aeiou]/gi); 
    const consonants = surname.match(/[^aeiou]/gi);
    let surnameCode = ""

    if ( surname.length < 3 ) {
        surnameCode += consonants[0]
        surnameCode += vowels[0]
        surnameCode += "X"
    } else if (consonants.length >= 3) {
        for (let i = 0; i < 3; i++) {
            surnameCode += consonants[i]
        }
    } else {
        if (consonants.length === 1) {
            surnameCode += consonants[0]
            surnameCode += vowels[0] + vowels[1]
        } else {
            surnameCode += consonants[0] + consonants[1]
            surnameCode += vowels[0]
        }
    }

    return surnameCode.toUpperCase()
}

/**
 * Takes a name and returns a fiscal code of the name
 * @param name 
 * @returns nameCode string with name code portion
 */
function fiscalName(name) {
    const vowels = name.match(/[aeiou]/gi); 
    const consonants = name.match(/[^aeiou]/gi);
    let nameCode = ""

    if ( name.length < 3 ) {
        nameCode += consonants[0]
        nameCode += vowels[0]
        nameCode += "X"
    } 
    else if (consonants.length === 3) {
        for (let i = 0; i < 3; i++) {
            nameCode += consonants[i]
        }
    } 
    else if (consonants.length > 3) {
        for (let i = 0; i < 4; i++) {
            if(i !== 1) {
                nameCode += consonants[i]
            }
        }
    }
    else {
        if (consonants.length === 1) {
            nameCode += consonants[0]
            nameCode += vowels[0] + vowels[1]
        } else {
            nameCode += consonants[0] + consonants[1]
            nameCode += vowels[0]
        }
    }

    return nameCode.toUpperCase()
}

/**
 * Takes the date of birth and the gender and transforms it into a fiscal code.
 * @param dob 
 * @param gender 
 * @returns dobCode string of the dob code portion
 */
function fiscalDob(dob, gender) {
    const dobArray = dob.split("/")
    const monthNumber = dobArray[1]
    let dobCode = ""

    dobCode += dobArray[2].slice(-2)

    dobCode += MONTHS[monthNumber]

    if ( gender === "F") {
        dobCode += (40 + Number(dobArray[0]))
    } 

    if (gender === "M") {
        if (dobArray[0] < 10) {
            dobCode += "0" + dobArray[0]
        } else {
            dobCode += dobArray[0]
        }
    }
    return dobCode
}