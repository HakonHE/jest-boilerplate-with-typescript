import { fiscalCoded } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('fiscalCoded()', () => {
    it('should return a string of "DBTMTT00A01"', function() { 
        const input = { 
            name: "Matt", 
            surname: "Edabit", 
            gender: "M", 
            dob: "1/1/1900" 
          }
        const output = "DBTMTT00A01"
        
        expect(fiscalCoded(input)).toEqual(output)
    }) 
    
    it('should return a string of "YUXHLN50T41"', function() { 
        const input = { 
            name: "Helen", 
            surname: "Yu", 
            gender: "F", 
            dob: "1/12/1950" 
          }
        const output = "YUXHLN50T41"
        
        expect(fiscalCoded(input)).toEqual(output)
    }) 
    
    it('should return a string of "MSOMKY28A16"', function() { 
        const input = { 
            name: "Mickey", 
            surname: "Mouse", 
            gender: "M", 
            dob: "16/1/1928" 
          }
        const output = "MSOMKY28A16"
        
        expect(fiscalCoded(input)).toEqual(output)
    }) 

    it('should return a string of "PSRCTS02L15"', function() { 
        const input = { 
            name: "Pusur", 
            surname: "Catsen", 
            gender: "M", 
            dob: "15/7/2002" 
          }
        const output = "CTSPSR02L15"
        
        expect(fiscalCoded(input)).toEqual(output)
    }) 
    
}) 


/*
fiscalCode({ 
  name: "Matt", 
  surname: "Edabit", 
  gender: "M", 
  dob: "1/1/1900" 
}) ➞ "DBTMTT00A01" 
 
fiscalCode({ 
  name: "Helen", 
  surname: "Yu", 
  gender: "F", 
  dob: "1/12/1950" 
}) ➞ "YUXHLN50T41" 
 
fiscalCode({ 
  name: "Mickey", 
  surname: "Mouse", 
  gender: "M", 
  dob: "16/1/1928" 
}) ➞ "MSOMKY28A16"
*/