export function passwordCheck(password) {
    const checks = {
        lowercase: false,
        uppercase: false,
        specialCharacter: false,
        goodLength: false,
        digit: false
    }
    let truthCounter = 0
    const passwordLength = password.length

    if (passwordLength < 6 || password.includes(" ")) return "Invalid"
    if (passwordLength >= 8) checks.goodLength = true
    if (containsSpecialChars(password)) checks.specialCharacter = true
    if (containsNumber(password)) checks.digit = true
    if (containsUppercase(password)) checks.uppercase = true
    if (containsLowercase(password)) checks.lowercase = true

    const checksArray = Object.values(checks)
    checksArray.forEach(item => {if(item === true) truthCounter++})

    if(checksArray.every(item => item === true)) {
        return "Strong"
    }
    else if (truthCounter >= 3) return "Moderate"
    

    

    return "Weak"
}

function containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(str);
}

function containsNumber(str) {
    const numbers = /\d/
    return numbers.test(str)
}

function containsUppercase(str) {
    const uppercase = /[A-Z]/
    return uppercase.test(str)
}

function containsLowercase(str) {
    const lowercase = /[a-z]/
    return lowercase.test(str)
}