import { passwordCheck } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('passwordCheck()', () => {
    it('should return a string of "Strong"', function() { 
        const input = "@S3cur1ty"
        const output = "Strong"
        
        expect(passwordCheck(input)).toEqual(output)
    }) 
    
    it('should return a string of "Weak"', function() { 
        const input = "11081992"
        const output = "Weak"
        
        expect(passwordCheck(input)).toEqual(output)
    }) 
    
    it('should return a string of "Weak"', function() { 
        const input = "password"
        const output = "Weak"
        
        expect(passwordCheck(input)).toEqual(output)
    }) 
    
    it('should return a string of "Invalid"', function() {  
        const input = "pass word"
        const output = "Invalid"
        
        expect(passwordCheck(input)).toEqual(output)
    })
    
    it('should return a string of "Invalid"', function() { 
        const input = "stonk"
        const output = "Invalid"
        
        expect(passwordCheck(input)).toEqual(output)
    }) 

    it('should return a string of "Moderate"', function() { 
        const input = "mySecurePass123"
        const output = "Moderate"
        
        expect(passwordCheck(input)).toEqual(output)
    }) 

    it('should return a string of "Moderate"', function() { 
        const input = "!@!pass1"
        const output = "Moderate"
        
        expect(passwordCheck(input)).toEqual(output)
    }) 

    
}) 



/*
stonk -> “Invalid” 
pass word -> “Invalid” 
password -> “Weak” 
11081992 -> “Weak” 
mySecurePass123 -> “Moderate” 
!@!pass1 -> “Moderate” 
@S3cur1ty -> “Strong”
*/