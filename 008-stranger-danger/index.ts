export function strangerDanger(textString) {
    let wordCount = 1
    textString = textString.toLowerCase()
    textString = textString.replaceAll(".", "")
    let wordArray = textString.split(" ")
    const completeArray = [[],[]]
    wordArray = wordArray.sort()
    for (let i = 1; i < wordArray.length; i++) {
        if(wordArray[i-1] !== wordArray[i]) {
            if(wordCount >= 5) {
                completeArray[1].push(wordArray[i-1])
            }
            else if (wordCount >= 3) {
                completeArray[0].push(wordArray[i-1])
            }
            wordCount = 1
        }
        wordCount++
    }


    return completeArray
}