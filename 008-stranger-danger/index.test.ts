
import { strangerDanger } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('strangerDanger()', () => {
    it('should return a an array of two arrays with the output [["spot", "see"], []]', function() { 
        const expectedValue = [["spot", "see"], []]
        
        expect(strangerDanger(
            "See Spot run. See Spot jump. Spot likes jumping. See Spot fly."))
            .toEqual(expectedValue)
    })
}) 

/*
noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot 
fly.") ➞ [["spot", "see"], []]
*/