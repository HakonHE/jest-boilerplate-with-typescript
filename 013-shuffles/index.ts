/**
 * Returns the number of shuffles it takes to return to original
 * @param Number cardAmount 
 * @returns 
 */
export function shufflesCount(cardAmount) {
    let startDeck = []
    for (let i = 0; i < cardAmount; i++) {
        startDeck.push(i+1)
    }
    const midDeckIndex = cardAmount / 2
    let shuffledDeck = []
    let shufflesDone = 0

    shufflesDone = (shuffling(startDeck, shuffledDeck, midDeckIndex, shufflesDone))

    return shufflesDone
}

/**
 * A recursive function which checks if the shuffledDeck is the same
 * as the starting deck, if not then it shuffles
 * @param NumberArray startDeck 
 * @param NumberArray shuffledDeck 
 * @param Number midDeckIndex 
 * @param Number shuffles 
 * @returns Number shuffles
 */
function shuffling(startDeck, shuffledDeck, midDeckIndex, shuffles) {
    let endDeck = []
    let startOfDeck = []
    if (shuffledDeck.length === 0) {
        endDeck = startDeck.slice(-midDeckIndex)
        startOfDeck = startDeck.slice(0, midDeckIndex)
    } else {
        endDeck = shuffledDeck.slice(-midDeckIndex)
        startOfDeck = shuffledDeck.slice(0, midDeckIndex)
    }
    
    if( checkArraysAreEqual(startDeck, shuffledDeck) ) {
        return shuffles
    } else {
        shuffledDeck = []
        shuffles++
        for (let i = 0; i < startOfDeck.length; i++) {
            shuffledDeck.push(startOfDeck[i])
            shuffledDeck.push(endDeck[i])
        }
        return shuffling(startDeck, shuffledDeck, midDeckIndex, shuffles)
    }
}

/**
 * Checks if the arrays are alike
 * @param arrayA 
 * @param arrayB 
 * @returns Boolean
 */
function checkArraysAreEqual(arrayA, arrayB) {
    if (!Array.isArray(arrayA) || !Array.isArray(arrayB)) {
      return false;
    } else if (arrayA === arrayB) {
      return true;
    } else if (arrayA.length !== arrayB.length) {
      return false;
    } else {
      for (let x = 0; x < arrayA.length; ++x) {
        if (arrayA[x] !== arrayB[x]) return false;
      }
      return true;
    }
  }