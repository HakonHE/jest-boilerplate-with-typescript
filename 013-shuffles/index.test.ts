import { shufflesCount } from "./index" // ES Module

// TESTING CODE USE npm test ./<name-of-task>/

// TEST CODE
describe('shufflesCount()', () => {
    it('should return the number 3', function() { 
        const input = 8
        const output = 3
        
        expect(shufflesCount(input)).toEqual(output)
    }) 
    
    it('should return the number 12', function() { 
        const input = 14
        const output = 12
        
        expect(shufflesCount(input)).toEqual(output)
    }) 
    
    it('should return the number 8', function() { 
        const input = 52
        const output = 8
        
        expect(shufflesCount(input)).toEqual(output)
    }) 
}) 

/*
shuffleCount(8) ➞ 3 
 
shuffleCount(14) ➞ 12 
 
shuffleCount(52) ➞ 8
*/